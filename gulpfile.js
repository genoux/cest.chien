var gulp = require('gulp');
const {
	src,
	dest
} = require('gulp');
const terser = require('gulp-terser');
var nodemon = require('gulp-nodemon');
var browserify = require('gulp-browserify');
var rename = require('gulp-rename');
/*function uglify() {
	return src(['./public/js/library/*.js', './public/js/core/*.js'])
		.pipe(babel())
		.pipe(concat('bundle.js'))
		.pipe(terser())
		.pipe(dest('public/js/'));
}*/

function browserifyBuild() {
	return src('./public/js/core/main.js')
		.pipe(browserify())
		.pipe(rename('bundle.js'))
		.pipe(terser())
		.pipe(dest('public/js/'));
}

function server() {
	return nodemon({
			ignore: [
				"public/js/bundle.js"
			],
			script: 'keystone.js',
		})
		.on('restart', ['default'], function () {
			var now = new Date();
			console.log('server is restarting: ' + now.getHours() + ' Hours, ' + now.getMinutes() + ' Minutes, ' + now.getSeconds() + ' Seconds');
			browserifyBuild();
		}).on('crash', function () {
			console.error('Application has crashed!\n')
			stream.emit('restart', 10) // restart the server in 10 seconds
		})
}


exports.server = server;
exports.browserifyBuild = browserifyBuild;

var build = gulp.series(browserifyBuild, server);
gulp.task('default', build);
