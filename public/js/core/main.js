var $ = require("jquery");
global.jQuery = require('jquery');
var AOS = require("aos")
var RGBaster = require("../library/rgbaster.min.js")
var tinycolor = require("tinycolor2");
var validator = require('validator');
require("../library/svg-inject");
require("bigslide");
require("jquery-backstretch");
require("jquery.scrollto")
require("../version");



$(document).ready(function () {
	function asyncFunction(item, cb) {
		setTimeout(function () {
			cb();
		}, 100);
	}


	function init() {
		SVGInject(document.querySelectorAll("img.svg-img"));
		$(".hamburger").bigSlide({
			menuWidth: "18em",
			speed: "250",
			easyClose: true,
			activeBtn: "navOpenBtn",
			afterOpen: function () {
				$(".hamburger").addClass("is-active");
			},
			afterClose: function () {
				$(".hamburger").removeClass("is-active");
			}
		});

		var dataBg = [].slice.call(document.querySelectorAll("div[data-bg]"));
		let backstretchImg = dataBg.map(function (item) {

			return new Promise(function (resolve) {
				$(item).backstretch($(item).data().bg, {
					fade: 200
				});
				asyncFunction(item, resolve);
			});
		})
		Promise.all(backstretchImg).then(function () {
			AOS.init({
				disable: false,
				duration: 500,
				once: true
			});
			pageConditions();
		});
	}

	function pageConditions() {
		if (window.location.href.indexOf("/services") != -1) {
			serviceInit();
		}
		if (
			window.location.href.indexOf("/blogue") != -1 &&
			window.location.href.indexOf("/post") == -1
		) {
			setTimeout(function () {
				blogInit();
			}, 300);
		}
		if (window.location.href.indexOf("/contact") != -1) {
			contactInit();
		}
	}

	function contactInit() {
		var confirmation = {
			name: false,
			email: false,
			selection: false,
			message: false
		};
		$("form").on("keyup change paste", "input, select, textarea", function () {
			if ($(".nameBox").val() != "") {
				confirmation.name = true;
			} else {
				confirmation.name = false;
			}
			//------------------------------------------------
			if (validator.isEmail($(".emailBox").val())) {
				confirmation.email = true;
			} else {
				confirmation.email = false;
			}
			//------------------------------------------------
			if ($(".selectionBox option:selected").val() != "") {
				confirmation.selection = true;
			} else {
				confirmation.selection = false;
			}
			//------------------------------------------------
			if ($(".messageBox").val() != "") {
				confirmation.message = true;
			} else {
				confirmation.message = false;
			}

			if (
				confirmation.name &&
				confirmation.email &&
				confirmation.message &&
				confirmation.selection
			) {
				$("form .form-submit button").removeClass("disabled");
				$("form").submit(function (event) {
					$("form .form-submit button").addClass("disabled");
					setTimeout(function () {
						$("form :input").prop("disabled", true);
						$("form").animate({
								opacity: 0.5
							},
							150
						);
					}, 200);
					//event.preventDefault();
				});
			} else {
				$("form .form-submit button").addClass("disabled");
			}
		});
	}

	function serviceInit() {
		var linkData = location.href;
		var anchor = linkData.substring(linkData.indexOf("?") + 1);
		// var lat = locals.anchorView;
		switch (anchor) {
			case "0301":
				$(window).scrollTo("#0301", 200, {
					offset: 50
				});
				break;
			case "0302":
				$(window).scrollTo("#0302", 200, {
					offset: 50
				});
				break;
			case "0303":
				$(window).scrollTo("#0303", 200, {
					offset: 50
				});
				break;
			case "null":
				$(window).scrollTo("top", 200);
				break;
			default:
				break;
		}
	}

	function msieversion() {

		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE ");
		var isIE = null;
		if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer, return version number
		{
			isIE = true;
		} else // If another browser, return 0
		{
			isIE = false;
		}

		return isIE;
	}

	function blogInit() {
		function asyncFunction(e, cb) {

			setTimeout(function () {
				$(e).animate({
					opacity: 1
				}, 150);
				$("#loader").fadeOut(500);
				cb();
			}, 100);
		}
		var post = [].slice.call(document.querySelectorAll(".post"));

		let getData = post.map(function (e, i) {

			return new Promise(function (resolve) {
				const imgSource = $(e)
					.children("img")
					.attr("src");
				if (msieversion()) {
					const $filter = $(e).children(".filter-post");
					const $blogBtn = $(e).children(".btn");
					$filter.css({
						background: "linear-gradient(135deg, rgba(18, 23, 46, 1) 0%, rgba(18, 23, 46, 0) 100%)"
					});
					$blogBtn.css({
						color: 'rgba(0, 84, 26,1)'
					});
					asyncFunction(e, resolve);
					return;
				} else {
					RGBaster.colors(imgSource, {
						paletteSize: 2,
						exclude: ["rgb(255,255,255)"],
						success: function (payload) {
							if (tinycolor(payload.dominant).isLight())
								payload.dominant = tinycolor(payload.dominant)
								.darken(32)
								.toString();
							if (tinycolor(payload.dominant).isDark())
								payload.dominant = tinycolor(payload.dominant)
								.lighten()
								.toString();

							const $filter = $(e).children(".filter-post");
							const $blogBtn = $(e).children(".btn");

							$blogBtn.css({
								color: payload.dominant
							});
							$filter.css({
								background: "linear-gradient(135deg," +
									tinycolor(payload.dominant)
									.darken(32)
									.toString() +
									" 0%," +
									tinycolor(payload.dominant)
									.setAlpha(0.2)
									.lighten(25)
									.toString() +
									" 100%)"
							});
							asyncFunction(e, resolve);
						}

					});
				}
			});
		})
		Promise.all(getData).then(function () {
			console.log('Done loading blog...')
			$('body').css('opacity', 1);
		});
	}
	init();
});
