$(window).on('load', function () {
	$.ajax({
		url: '/version',
		complete: function (data) {
			var appData = data.responseJSON
			console.group(
				"%c " + appData.author + " was here...",
				"color: #2a4fd3; font-weight: 800; font-size:16px;"
			);
			console.log(appData.name);
			console.log('Version: ' + appData.version);
			console.log("www.barrymind.ca");
			console.log("john.olivierb@gmail.com");
			console.log("✌️");
			console.groupEnd();
		}
	});
});
