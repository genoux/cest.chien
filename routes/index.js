/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require("keystone");
var middleware = require("./middleware");
var importRoutes = keystone.importer(__dirname);
// Common Middleware
keystone.pre("routes", middleware.initLocals);
keystone.pre("render", middleware.flashMessages);
var fs = require("fs");


// Import Route Controllers
var routes = {
	views: importRoutes("./views")
};


// Setup Route Bindings
exports = module.exports = function (app) {
	// Views
	app.get("/", routes.views.index);
	app.get("/apropos", routes.views.apropos);
	app.get("/services", routes.views.services);
	app.all("/contact", routes.views.contact);
	app.get("/blogue/:category?", routes.views.blog);
	app.get("/blogue/post/:post", routes.views.post);
	app.get("/admin", function (req, res) {
		res.redirect("/keystone");
	});
	app.get('/version', function (req, res, next) {
		var content = fs.readFileSync("./package.json");
		res.json(JSON.parse(content));
	});
	app.get("/sitemap.xml", function (req, res) {
		console.log('req', req);
		res.setHeader('Content-Type', 'text/xml');
		res.end(fs.readFileSync('./sitemap.xml', {
			encoding: 'utf-8'
		}));
	});
	app.get("/robots.txt", function (req, res) {
		console.log('req', req);
		res.setHeader('Content-Type', 'text/plain');
		res.end(fs.readFileSync('./robots.txt', {
			encoding: 'utf-8'
		}));
	});
	app.use(function (req, res) {
		res.set('location', 'https://cestchien.ca');
		res.status(301).send()
		res.render("404.pug");
	});




};
