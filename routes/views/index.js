var keystone = require('keystone'),
	pjson = require("../../package.json");
exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'accueil';

	// Render the view
	view.render('index', {
		title: 'Accueil',
		description: 'Nous sommes une équipe d’intervenantes en comportement animal et entraîneuses',
		app: pjson
	});
};
