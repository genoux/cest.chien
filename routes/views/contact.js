var keystone = require('keystone');
var Enquiry = keystone.list('Enquiry');
const nodemailer = require('nodemailer');

exports = module.exports = function (req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;
	var transport = nodemailer.createTransport({
		//host: 'smtp.sendgrid.net',
		service: "Hotmail",
		//port: 465,
		//secure: true, // true for 465, false for other ports 587
		auth: {
			user: 'compagniecestchien@hotmail.com', // generated ethereal user
			pass: global.env.HOTMAIL_PASS, // generated ethereal password
		}
	});
	// Set locals
	locals.section = 'contact';
	locals.enquiryTypes = Enquiry.fields.enquiryType.ops;
	locals.formData = req.body || {};
	locals.validationErrors = {};


	// On POST requests, add the Enquiry item to the database
	view.on('post', {
		action: 'contact'
	}, function (next) {
		//var text = locals.formData.message.replace(/\s/g, '&nbsp;');
		let mailHtml = '<div class="mess" style="margin:20px;">\
		<h1 style="font-size:16px;margin-bottom:16px;">' + locals.formData.namefull + ' - ' + locals.formData.email + '</h1>\
		<div class="elm" style="margin:10px;">\
			<h2 style="font-size:15px;font-weight:bold;margin:0px 10px 0px 0px;padding-left:10px;float:left;border-left:1px solid black;">Téléphone:</h2>\
			<p style="font-size:14px; margin:0px 10px;">' + locals.formData.phone + '</p>\
		</div>\
		<div class="elm" style="margin:10px;">\
			<h2 style="font-size:15px;font-weight:bold;margin:0px 10px 0px 0px;padding-left:10px;float:left;border-left:1px solid black;">Ville:</h2>\
			<p style="font-size:14px; margin:0px 10px;">' + locals.formData.enquiryType + '</p>\
		</div>\
		<div class="elm" style="margin:10px;">\
			<h2 style="font-size:15px;font-weight:bold;margin:0px;padding-left:10px;border-left:1px solid black;">Message:</h2>\
			<pre style="font-size:14px; margin-top:10px;">' + locals.formData.message + '</pre>\
		</div>\
		</div>';
		const mailOptions = {
			from: 'compagniecestchien@hotmail.com',
			to: 'compagniecestchien@hotmail.com', // list of receivers
			sender: locals.formData.email,
			headers: {
				'My-Custom-Header': 'header value'
			},
			replyTo: locals.formData.email,
			subject: locals.formData.namefull + ' - ' + locals.formData.email, // Subject line
			html: mailHtml // html body
		};

		transport.sendMail(mailOptions, function (error) {
			if (error) {
				console.log(error);
				req.flash('error', 'Oups, le message n\'a pas été envoyé, veuillez réessayer!');
				res.redirect('contact')
			} else {
				console.log('Message sent successfully!');
				req.flash('success', 'Super, votre message à été envoyé!');
				res.redirect('contact')
			}
			transport.close();
		});

	});
	view.render('contact', {
		title: 'Contact',
		description: 'Pour prendre rendez-vous ou pour toute autre demande',
	});
};
