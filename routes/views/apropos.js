var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'apropos';

	// Render the view
	view.render('apropos', {
		title: 'À propos',
		description: 'Nos valeurs prédominantes sont la rigueur, la fiabilité, la coopération, le soutien mais surtout le plaisir!',
	});
};
