// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
let secureEnv = require('secure-env');
global.env = secureEnv({
	secret: 'fzYPYBh8WGjut6T6'
})
require('dotenv').config();
// Require keystone
var keystone = require('keystone');

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({
	'name': 'C\'est Chien',
	'brand': 'C\'est Chien',
	'cloudinary config': global.env.CLOUDINARY_URL,
	'cookie secret': global.env.COOKIE_SECRET,
	port: global.env.PORT,
	'sass': 'public',
	'static': ['public', 'node_module'],
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'pug',
	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',
	'wysiwyg override toolbar': false,
	'wysiwyg menubar': true,
	'wysiwyg skin': 'lightgray',
	'wysiwyg additional buttons': 'searchreplace visualchars,' +
		' charmap ltr rtl pagebreak paste, forecolor backcolor,' +
		' emoticons media, preview print ' +
		' visualblocks, visualchars, wordcount'
});

// Load your project's Models
keystone.import('models');

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js
keystone.set('locals', {
	_: require('lodash'),
	//env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable,
});

// Load your project's Routes
keystone.set('routes', require('./routes'));

// Configure the navigation bar in Keystone's Admin UI
keystone.set('nav', {
	posts: ['posts', 'post-categories'],
	//enquiries: 'enquiries',
	users: 'users',
});

keystone.set('cloudinary secure', true);
keystone.set('signin logo', '/images/logo-green.svg');
keystone.start();
